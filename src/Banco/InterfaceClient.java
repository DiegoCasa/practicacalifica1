package Banco;

public interface InterfaceClient {
	
	public String name = "";
	public String address = "";
	public int typeClient = 0;
	public int time = 0;
	
	public String getName();
	public String getAddress();
	public int getTypeClient();
	public int getTime();

}
