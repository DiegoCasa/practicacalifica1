package Banco;

public interface InterfaceATM {
	
	public String name = "";
	public String address = "";
	public int typeATM = 0;
	
	public String getName();
	public String getAddress();
	public int getTypeATM();

}
