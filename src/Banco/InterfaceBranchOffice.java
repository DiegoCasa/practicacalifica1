package Banco;

public interface InterfaceBranchOffice {
	
	public String name = "";
	public String address = "";
	public String phone = "";
	
	public String getName();
	public String getAddress();
	public String getPhone();

}
